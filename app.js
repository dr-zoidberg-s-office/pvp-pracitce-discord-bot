const { Client, Collection, Intents } = require('discord.js');
const fs = require('fs');
const { Worker } = require('worker_threads')

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
client.commands = new Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.data.name, command);
}
client.once('ready', () => {
	console.log('Ready!');
});




client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return;

    if (!client.commands.has(interaction.commandName)) return;

	try {
		await client.commands.get(interaction.commandName).execute(interaction);
	} catch (error) {
		console.error(error);
		await interaction.reply({ content: 'There was an error while executing this command! You mistyped the id or have misspelled an option.', ephemeral: true });
		}
});


// This code block is reserved for future commands. This code enables for taks to be run on a "second" thread. I am not a Javascrpt developer by trade, so I just use setTimeout

// function garbagecleaner() {
// 	// Do regular cleanup code
// 	setTimeout(garbagecleaner, 4000)
// }
// setTimeout(garbagecleaner,5000)

let token = ""
client.login(token)
