const { SlashCommandBuilder } = require('@discordjs/builders');
const { TextChannel } = require('discord.js');
const { Message } = require('discord.js');
const { MessageEmbed } = require('discord.js');
const { CommandInteraction } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('suggestreply')
        .addStringOption(option =>
            option.setName('comfirmation')
                .setDescription('Accept or deny')
                .addChoice("accept", "accept")
                .addChoice("deny", "accept")
                .setRequired(true)
        )
        .addStringOption(option => 
            option.setName("messageid")
            .setDescription("Message id")
            .setRequired(true)
            )
            .addStringOption(option =>
                option.setName('reason')
                    .setDescription('The reason')

                    .setRequired(true)
            )

		.setDescription('Accepts a suggestion'),
	async execute(interaction) {



        /** @type {CommandInteraction} */
        var intent = interaction;



        let guildMem = await intent.guild.members.fetch(intent.member.id)

        let roles = guildMem.roles.cache.filter(role => role.id === '885575059018764378')

        if (roles.entries() === 1) {

            await intent.reply("GET OUT!")
            return

        }
        // Type hints for VSCode
        /** @type {TextChannel} */
        let suggestionsChn =  intent.guild.channels.cache.find(channel => channel.id === "874425556232462356")
               /** @type {Message} */
        let msg = await suggestionsChn.messages.fetch(intent.options.getString("messageid").trim())
        if (msg === undefined) {
            await intent.reply("No message found with that id")
            return

        }
        if (intent.options.getString("comfirmation") === "accept") {
            let acceptEmbed = new MessageEmbed()
            .setColor('#008000')
            .setTitle("Suggestion **Accepted**")
            .addField("**Reason**", intent.options.getString("reason"))
            .setDescription(("The suggestion has been accepted by @"  + intent.user.username))
            .setAuthor(intent.user.username, intent.user.avatarURL())
            msg.reply({embeds: [acceptEmbed]})
        } else if (intent.options.getString("comfirmation") === "deny")  {
            let acceptEmbed = new MessageEmbed()
            .setColor('#c70039')
            .setTitle("Suggestion **Denied**")
            .addField("**Reason**", intent.options.getString("reason"))

            .setDescription(("The suggestion has been denied by @"  + intent.user.username))
            .setAuthor(intent.user.username, intent.user.avatarURL())
            msg.reply({embeds: [acceptEmbed]})

        }

        await intent.reply({content: "Suggestion Accepted!", ephemeral: true})

	},
};