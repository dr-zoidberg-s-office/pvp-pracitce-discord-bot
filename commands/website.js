const { SlashCommandBuilder } = require('@discordjs/builders');
const { CommandInteraction } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('website')
		.setDescription('Sends you to the PVP Practice forums'),
	async execute(interaction) {
        /** @type {CommandInteraction} */
        var intent = interaction;
		await intent.reply("https://pvp-practice.net")
	},
};