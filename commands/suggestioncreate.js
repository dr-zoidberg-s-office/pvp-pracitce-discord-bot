const { SlashCommandBuilder } = require('@discordjs/builders');
const { GuildChannel } = require('discord.js');
const { Message } = require('discord.js');
const { MessageEmbed } = require('discord.js');
const { CommandInteraction } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('suggest')
        .addStringOption(option => 
            option.setRequired(true)
            .setName("message")
            .setDescription("The suggestion you want to make")
        )
		.setDescription('Makes a suggestion'),
	async execute(interaction) {
        /** @type {CommandInteraction} */
        var intent = interaction;
        // Removed due to @UltimateProgrammer requesting a bug fix

        // if ((intent.channel.id === '898634223060152370') == false) {
        //     console.log("hi")
        //     await intent.reply("You must make a suggestion in #make-a-suggestion")
        //     return

        // }
        console.log(intent.channel.id)


        /** @type {GuildChannel} */
       let suggestionsChn =  intent.guild.channels.cache.find(channel => channel.id === "874425556232462356")
       let suggestionEmbed = new MessageEmbed()
       .setColor('#e4d00a')
       .setTitle("Suggestion Added")
       .setDescription(("A suggestion from " + intent.user.username))
       .addField("Suggestion:", intent.options.getString("message"))
       .setAuthor(intent.user.username, intent.user.avatarURL())
               /** @type {Message} */
      let message = await suggestionsChn.send({embeds: [suggestionEmbed]})
      message.react("⬆️")
      message.react("⬇️")
      await intent.reply({content: "Your suggestion is posted", ephemeral: true})
    },
};