const { SlashCommandBuilder } = require('@discordjs/builders');
const { CommandInteraction } = require('discord.js');
const { Intents } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('ping')
        .addStringOption(option =>
            option.setName('input')
                .setDescription('The input to echo back')
                .setRequired(true))
		.setDescription('Replies with Pong!'),
	async execute(interaction) {
        /** @type {CommandInteraction} */
        var intent = interaction;
        intent.options.getString("")
		await intent.reply("Haha no ping for you")
	},
};